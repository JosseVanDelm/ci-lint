import yaml
import json
import requests
import argparse

parser = argparse.ArgumentParser(description='Perform gitlab-ci lint.')

parser.add_argument('--input', help='path to .gitlab-ci.yml',
                    dest='input_file',
                    default='.gitlab-ci.yml')
parser.add_argument('--token', help='string with Gitlab access token',
                    default='add your default token here',
                    dest='token')
parser.add_argument('--url', help='url to gitlab instance',
                    dest='instance_url',
                    default="https://gitlab.com")

args = parser.parse_args()

input_file = args.input_file

with open(input_file) as input_file:
    try:
        ci_yaml = yaml.load(input_file, Loader=yaml.FullLoader)
    except yaml.scanner.ScannerError as e:
        print(f"YAML invalid: {e}")
        exit(1)
    ci_json = json.dumps(ci_yaml)

url = args.instance_url + "/api/v4/ci/lint"
token = args.token
headers = {
    'PRIVATE-TOKEN': token,
}

json_data = {'content': ci_json}
try:
    response = requests.post(url, headers=headers, json=json_data)
except Exception as e:
    print(f"HTTPS Error: {e}")
    exit(2)
print(json.dumps(json.loads(response.content)))
